# snowplow-docker

Public Docker builds for AWS ECS

Forked from <https://github.com/snowplow-starter-aws>

## Local dev

```bash
docker-compose up
```
